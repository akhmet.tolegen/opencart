<?php
// Heading
$_['heading_title']		 = 'hbepay';

// Text
$_['text_extension']	 = 'Extensions';
$_['text_success']		 = 'Success: You have modified hbepay account details!';
$_['text_edit']          = 'Edit hbepay';
$_['text_hbepay']		 = '<img src="view/image/payment/hbepay.png" alt="hbepay" title="hbepay" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_username']	 = 'hbepay Username';
$_['entry_password']	 = 'Password';
$_['entry_test']		 = 'Test Mode';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Order Status';
$_['entry_geo_zone']	 = 'Geo Zone';
$_['entry_status']		 = 'Status';
$_['entry_sort_order']	 = 'Sort Order';

// Help
$_['help_password']		 = 'Just use some random password. This will be used to make sure the payment information is not interfered with after being sent to the payment gateway.';
$_['help_total']		 = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']	 = 'Warning: You do not have permission to modify payment hbepay!';
$_['error_username']	 = 'hbepay Username required!';
$_['error_password']	 = 'Password required!';